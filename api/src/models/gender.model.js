module.exports = (sequelize, Sequelize) => {
    var genders = sequelize.define('genders', {
    id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
    },
    population: {
          type: Sequelize.STRING,
          allowNull: false    
    }
  },
  {
    timestamps: false
  });
  return genders;
}
